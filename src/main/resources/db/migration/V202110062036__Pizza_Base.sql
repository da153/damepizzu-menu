CREATE TABLE "menu"."pizza_base"
(
    "id"          uuid              NOT NULL,
    "name"        character varying NOT NULL,
    "description" character varying,
    "extra_price" numeric           NOT NULL default 0,
    CONSTRAINT "pizza_base_pk" PRIMARY KEY ("id")
) WITH (oids = false);
