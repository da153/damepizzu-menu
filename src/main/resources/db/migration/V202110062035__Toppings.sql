DROP TABLE IF EXISTS "menu"."topping";

CREATE TABLE "menu"."topping"
(
    "id"          uuid              NOT NULL,
    "name"        character varying NOT NULL,
    "description" character varying,
    "price"       numeric           NOT NULL default 0,
    "image_id"    uuid,
    CONSTRAINT "topping_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

ALTER TABLE "menu"."topping"
    ADD FOREIGN KEY ("image_id") REFERENCES "file" ("id") ON DELETE CASCADE;

