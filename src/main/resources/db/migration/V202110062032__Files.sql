DROP TYPE IF EXISTS "menu"."file_type";
CREATE TYPE "menu"."file_type" AS ENUM ('image', 'file');
CREATE CAST (character varying AS file_type) WITH INOUT AS ASSIGNMENT;


CREATE TABLE "menu"."file"
(
    "id"    uuid              NOT NULL,
    "name"  character varying NOT NULL,
    "size"  numeric           NOT NULL,
    "type"  file_type         NOT NULL,
    "e_tag" character varying NOT NULL,
    "hash"  numeric           NOT NULL,
    CONSTRAINT "file_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

ALTER TABLE "menu"."file"
    ADD CONSTRAINT "file_hash_type" UNIQUE ("hash", "type");
