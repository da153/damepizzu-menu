ALTER TABLE "menu"."pizza"
    ADD COLUMN "pizza_base_id" uuid;

ALTER TABLE "menu"."pizza"
    ADD FOREIGN KEY ("pizza_base_id") REFERENCES "menu"."pizza_base" ("id") ON DELETE CASCADE;
