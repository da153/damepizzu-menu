DROP TABLE IF EXISTS "menu"."pizza";

CREATE TABLE "menu"."pizza"
(
    "id"          uuid              NOT NULL,
    "name"        character varying NOT NULL,
    "description" character varying,
    "price"       numeric           NOT NULL,
    "image_id"    uuid,
    CONSTRAINT "pizza_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

ALTER TABLE "menu"."pizza"
    ADD FOREIGN KEY ("image_id") REFERENCES "menu"."file" ("id") ON DELETE CASCADE;

