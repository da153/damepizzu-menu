CREATE TABLE "menu"."pizza_topping"
(
    "pizza_id"  uuid NOT NULL,
    "topping_id" uuid NOT NULL,
    CONSTRAINT "pizza_topping_pk" PRIMARY KEY ("pizza_id", "topping_id")
) WITH (oids = false);


ALTER TABLE "menu"."pizza_topping"
    ADD FOREIGN KEY ("pizza_id") REFERENCES "menu"."pizza" ("id") ON DELETE CASCADE;


ALTER TABLE "menu"."pizza_topping"
    ADD FOREIGN KEY ("topping_id") REFERENCES "menu"."topping" ("id") ON DELETE CASCADE;
