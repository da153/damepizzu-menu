package com.example.springsocial.repository;

import java.util.Optional;

public interface RepositoriesManager {

	<T> Optional<BaseRepository> getRepositoryOfType(Class<T> type);

}
