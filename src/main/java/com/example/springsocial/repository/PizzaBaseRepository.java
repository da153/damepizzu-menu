package com.example.springsocial.repository;

import com.example.springsocial.model.PizzaBase;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PizzaBaseRepository extends BaseRepository<PizzaBase, UUID> {

	Optional<PizzaBase> findByName(String name);

	List<PizzaBase> findAll(String search);
}
