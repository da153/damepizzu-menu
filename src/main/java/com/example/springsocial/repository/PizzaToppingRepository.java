package com.example.springsocial.repository;

import com.example.springsocial.model.PizzaTopping;
import com.example.springsocial.model.Topping;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PizzaToppingRepository extends BaseRepository<Topping, UUID> {

	Optional<Topping> findByName(String name);

	List<Topping> findAll(String search);
}
