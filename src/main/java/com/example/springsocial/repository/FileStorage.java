package com.example.springsocial.repository;

import com.example.springsocial.util.Tuple;
import io.minio.ObjectStat;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface FileStorage {

	String save(String fileName, byte[] file);

	ObjectStat save(String fileName, MultipartFile file);

	ObjectStat save(String fileName, InputStream inputStream, String type);

	void delete(String filepath);

	byte[] get(String filepath);

	Tuple<byte[], ObjectStat> getWithMetadata(String filepath);

}
