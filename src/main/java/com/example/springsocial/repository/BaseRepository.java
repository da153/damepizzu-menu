package com.example.springsocial.repository;

import com.example.springsocial.model.Pizza;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface BaseRepository<T, V> extends AutoSelectRepository<T> {

	T getById(V id);

	Optional<T> findById(V id);

	T getNotNull(V id);

	T save(T object);

	void deleteById(V id);

	void delete(T object);

	void clearCache();

	List<T> findAll();

	void clear();

}
