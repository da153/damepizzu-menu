package com.example.springsocial.repository.engine;

import com.example.springsocial.model.Pizza;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


public interface PizzaRepositoryEngine extends CrudRepository<Pizza, UUID> {

	@Query("SELECT p FROM Pizza p WHERE p.name LIKE ?1 OR p.description LIKE ?1")
	List<Pizza> findAll(String search);

	Optional<Pizza>findByName(String name);

	List<Pizza> findByIdIn(List<UUID> ids);

	boolean existsAllByIdIn(List<UUID> ids);


}
