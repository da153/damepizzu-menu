package com.example.springsocial.repository.engine;

import com.example.springsocial.model.PizzaBase;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PizzaBaseRepositoryEngine extends CrudRepository<PizzaBase, UUID> {

	public Optional<PizzaBase> findByName(String name);

	@Query("SELECT p FROM PizzaBase p WHERE p.name LIKE ?1 OR p.description LIKE ?1")
	public List<PizzaBase> findAll(String search);
}
