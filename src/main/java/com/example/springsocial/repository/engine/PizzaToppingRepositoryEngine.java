package com.example.springsocial.repository.engine;

import com.example.springsocial.model.Pizza;
import com.example.springsocial.model.PizzaTopping;
import com.example.springsocial.model.Topping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PizzaToppingRepositoryEngine extends CrudRepository<Topping, UUID> {
	public Optional<Topping> findByName(String name);

	@Query("SELECT t FROM Topping t WHERE t.name LIKE ?1 OR t.description LIKE ?1")
	public List<Topping> findAll(String search);
}

