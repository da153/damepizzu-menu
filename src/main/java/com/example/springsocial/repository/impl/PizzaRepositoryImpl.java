package com.example.springsocial.repository.impl;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.Pizza;
import com.example.springsocial.repository.BaseRepository;
import com.example.springsocial.repository.PizzaRepository;
import com.example.springsocial.repository.engine.PizzaRepositoryEngine;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@AllArgsConstructor
@Slf4j
public class PizzaRepositoryImpl extends AbstractRepository<Pizza, UUID> implements PizzaRepository {

	private static final String CACHE_NAME = "PIZZA";

	private final PizzaRepositoryEngine engine;

	@Override
	protected CrudRepository<Pizza, UUID> getRepositoryEngine() {
		return engine;
	}

	@Override
	@Cacheable(cacheNames = CACHE_NAME)
	public Pizza getNotNull(UUID id) {
		return super.getNotNull(id);
	}

	@Transactional
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	@Override
	public Pizza save(Pizza toSave) {
		return super.save(toSave);
	}

	@Override
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	public void deleteById(UUID id) {
		super.deleteById(id);
	}

	@Override
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	public void delete(Pizza object) {
		super.delete(object);
	}

	@Override
	@Cacheable(cacheNames = CACHE_NAME)
	public List<Pizza> findAll(String search) {
		return engine.findAll(formatSearchString(search));
	}

	@Override
	public Optional<Pizza> findByName(String name) {
		return engine.findByName(name);
	}

	@Override
	public List<Pizza> findByIdIn(List<UUID> ids) {
		return engine.findByIdIn(ids);
	}

	@Override
	public boolean existsAllByIdIn(List<UUID> ids) {
		List<Pizza> byIdIn = engine.findByIdIn(ids);
		if (byIdIn.size() == ids.size()) {
			return true;
		}
		throw new BadRequestException("Not all ids has been founded!");
	}

}
