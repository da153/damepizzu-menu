package com.example.springsocial.repository.impl;

import com.example.springsocial.model.PizzaTopping;
import com.example.springsocial.model.Topping;
import com.example.springsocial.repository.PizzaToppingRepository;
import com.example.springsocial.repository.engine.PizzaToppingRepositoryEngine;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class PizzaToppingRepositoryImpl extends AbstractRepository<Topping, UUID> implements PizzaToppingRepository {

	private static final String CACHE_NAME = "TOPPINGS";

	private final PizzaToppingRepositoryEngine engine;

	@Override
	@Cacheable(cacheNames = CACHE_NAME)
	public List<Topping> findAll() {
		return super.findAll();
	}

	@Override
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	public void deleteById(UUID id) {
		super.deleteById(id);
	}

	@Override
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	public void delete(Topping object) {
		super.delete(object);
	}

	@Transactional
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	@Override
	public Topping save(Topping toSave) {
		return super.save(toSave);
	}

	@Override
	protected CrudRepository<Topping, UUID> getRepositoryEngine() {
		return engine;
	}

	@Override
	public Optional<Topping> findByName(String name) {
		return engine.findByName(name);
	}

	@Override
	public List<Topping> findAll(String search) {
		return engine.findAll(formatSearchString(search));
	}

}
