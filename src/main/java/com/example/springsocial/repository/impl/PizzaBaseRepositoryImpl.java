package com.example.springsocial.repository.impl;

import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.repository.BaseRepository;
import com.example.springsocial.repository.PizzaBaseRepository;
import com.example.springsocial.repository.engine.PizzaBaseRepositoryEngine;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class PizzaBaseRepositoryImpl extends AbstractRepository<PizzaBase, UUID> implements PizzaBaseRepository {

	private static final String CACHE_NAME = "TOPPINGS";

	private final PizzaBaseRepositoryEngine engine;

	@Override
	@Cacheable(cacheNames = CACHE_NAME)
	public PizzaBase getNotNull(UUID id) {
		return super.getNotNull(id);
	}

	@Override
	@Cacheable(cacheNames = CACHE_NAME)
	public PizzaBase getById(UUID id) {
		return super.getById(id);
	}

	@Transactional
	@CacheEvict(value = CACHE_NAME, allEntries = true)
	@Override
	public PizzaBase save(PizzaBase toSave) {
		return super.save(toSave);
	}

	@Override
	protected CrudRepository<PizzaBase, UUID> getRepositoryEngine() {
		return engine;
	}

	@Override
	public Optional<PizzaBase> findByName(String name) {
		return engine.findByName(name);
	}

	@Override
	public List<PizzaBase> findAll(String search) {
		return engine.findAll(formatSearchString(search));
	}

}
