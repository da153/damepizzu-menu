package com.example.springsocial.repository.impl;

import com.example.springsocial.model.File;
import com.example.springsocial.repository.engine.ImageDbRepositoryEngine;
import com.example.springsocial.repository.model.PagingInfo;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
@Repository
public class ImageDbRepository {

	private final ImageDbRepositoryEngine engine;

	public File getNotNull(UUID id) {
		Optional<File> file = engine.findById(id);
		Assert.notNull(file.get(), "Cannot find desired object with id: " + id);
		return file.get();
	}

	public File save(File file) {
		return engine.save(file);
	}

	public void deleteById(UUID id) {
		engine.deleteById(id);
	}

	public List<File> findAll() {
		return (List<File>) engine.findAll();
	}

	public Page<File> findAllFiles(String query, PagingInfo pagingInfo) {
		return engine.findAll(pagingInfo.getPageable());
	}

}
