package com.example.springsocial.repository;

import com.example.springsocial.model.Pizza;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PizzaRepository extends BaseRepository<Pizza, UUID> {

	List<Pizza> findAll(String search);

	Optional<Pizza> findByName(String name);

	List<Pizza> findByIdIn(List<UUID> ids);

	boolean existsAllByIdIn(List<UUID> ids);



}
