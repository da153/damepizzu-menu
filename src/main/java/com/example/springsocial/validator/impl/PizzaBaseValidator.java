package com.example.springsocial.validator.impl;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.repository.PizzaBaseRepository;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PizzaBaseValidator implements Validator<PizzaBase> {

	private final PizzaBaseRepository pizzaBaseRepository;

	@Override
	public void validateBeforeCreation(PizzaBase pizzaBase) {
		pizzaBaseRepository.findByName(pizzaBase.getName()).ifPresent(p -> {
			throw new BadRequestException("Pizza base with this name already exists!");
		});
	}

	@Override
	public void validateBeforeEditing(PizzaBase pizzaBase) {
		pizzaBaseRepository.findByName(pizzaBase.getName()).ifPresent(p -> {
			if (!pizzaBase.getId().equals(p.getId())) {
				throw new BadRequestException("Pizza base with this name already exists!");
			}
		});
	}

}
