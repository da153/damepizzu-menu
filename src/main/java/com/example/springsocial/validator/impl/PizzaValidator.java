package com.example.springsocial.validator.impl;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.Pizza;
import com.example.springsocial.repository.PizzaRepository;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class PizzaValidator implements Validator<Pizza> {

	private final PizzaRepository pizzaRepository;

	@Override
	public void validateBeforeCreation(Pizza pizza) {
		pizzaRepository.findByName(pizza.getName()).ifPresent(p -> {
			throw new BadRequestException(String.format("Pizza with name: %s already exists!", pizza.getName()));
		});

	}

	@Override
	public void validateBeforeEditing(Pizza pizza) {
		pizzaRepository.findByName(pizza.getName()).ifPresent(p -> {
			if (!p.getId().equals(pizza.getId())) {
				throw new BadRequestException(String.format("Pizza with name: %s already exists!", pizza.getName()));
			}
		});
	}

	@Override
	public void validateBeforeOrdering(List<Pizza> object) {
		Validator.super.validateBeforeOrdering(object);
	}

}
