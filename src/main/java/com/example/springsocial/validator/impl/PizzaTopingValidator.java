package com.example.springsocial.validator.impl;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.Topping;
import com.example.springsocial.repository.PizzaToppingRepository;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PizzaTopingValidator implements Validator<Topping> {

	private final PizzaToppingRepository repository;

	@Override
	public void validateBeforeCreation(Topping topping) {
		repository.findByName(topping.getName()).ifPresent(p -> {
			throw new BadRequestException("Topping with name already exists!");
		});
	}

	@Override
	public void validateBeforeEditing(Topping topping) {
		repository.findByName(topping.getName()).ifPresent(p -> {
			if (!p.getId().equals(topping.getId())) {
				throw new BadRequestException("Topping with name already exists!");
			}
		});
	}

}
