package com.example.springsocial.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI().components(generateComponents())
			.info(generateInfo());
	}

	private Components generateComponents() {
		Components components = new Components();

		components.addSecuritySchemes("bearer",
			new SecurityScheme().type(SecurityScheme.Type.HTTP)
				.scheme("bearer")
				.bearerFormat("JWT")
		);

		return components;
	}

	private Info generateInfo() {
		Info info = new Info();
		info.title("DamePizzu");
		info.description("DamePizzu");
		info.setVersion("1.0.0");
		info.setContact(generateContact());
		return info;
	}

	private Contact generateContact() {
		Contact contact = new Contact();
		contact.setName("Damepizzu");
		return contact;
	}

}
