package com.example.springsocial.config;

import io.minio.MinioClient;
import io.minio.errors.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Configuration
public class MinioConfig {

	@Autowired
	private MinioConfigurationProperties configurationProperties;

	@Bean
	public MinioClient createClient() throws IOException, InvalidPortException, InvalidEndpointException, InvalidBucketNameException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException, RegionConflictException {
		MinioClient client = new MinioClient(configurationProperties.getUrl(),
			configurationProperties.getAccessKey(),
			configurationProperties.getSecretKey(),
			configurationProperties.getRegion()
		);
		if (!client.bucketExists(configurationProperties.getBucket())) {
			client.makeBucket(configurationProperties.getBucket());
		}
		return client;
	}

}
