package com.example.springsocial.service;

import com.example.springsocial.model.dto.PizzaToppingDto;
import com.example.springsocial.model.dto.request.NewPizzaToppingDto;

import java.util.List;
import java.util.UUID;

public interface PizzaToppingService {

	public List<PizzaToppingDto> findAll(String search);

	public PizzaToppingDto findById(UUID id);

	public PizzaToppingDto createNewTopping(NewPizzaToppingDto toppingDto);

	public PizzaToppingDto editTopping(NewPizzaToppingDto toppingDto, UUID id);

	public void deleteTopping(UUID id);

	public PizzaToppingDto changeImage(UUID id, UUID imageId);

	public PizzaToppingDto deleteImage(UUID id);
}
