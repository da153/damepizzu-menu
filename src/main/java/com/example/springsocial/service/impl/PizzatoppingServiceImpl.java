package com.example.springsocial.service.impl;

import com.example.springsocial.mapper.PizzaToppingMapper;
import com.example.springsocial.model.File;
import com.example.springsocial.model.Topping;
import com.example.springsocial.model.dto.PizzaToppingDto;
import com.example.springsocial.model.dto.request.NewPizzaToppingDto;
import com.example.springsocial.repository.PizzaToppingRepository;
import com.example.springsocial.repository.impl.ImageDbRepository;
import com.example.springsocial.service.PizzaToppingService;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class PizzatoppingServiceImpl implements PizzaToppingService {

	private final PizzaToppingRepository pizzaToppingRepository;

	private final PizzaToppingMapper pizzaToppingMapper;

	private final ImageDbRepository imageDbRepository;

	private final Validator<Topping> validator;

	@Override
	public List<PizzaToppingDto> findAll(String search) {
		List<Topping> toppings = pizzaToppingRepository.findAll(search);
		return pizzaToppingMapper.entitiesToDtos(toppings);
	}

	@Override
	public PizzaToppingDto findById(UUID id) {
		Topping topping = pizzaToppingRepository.getNotNull(id);
		return pizzaToppingMapper.entityToDto(topping);
	}

	@Override
	public PizzaToppingDto createNewTopping(NewPizzaToppingDto toppingDto) {
		Topping topping = pizzaToppingMapper.createNewTopping(toppingDto);
		validator.validateBeforeCreation(topping);
		return pizzaToppingMapper.entityToDto(pizzaToppingRepository.save(topping));
	}

	@Override
	public PizzaToppingDto editTopping(NewPizzaToppingDto toppingDto, UUID id) {
		Topping toEdit = pizzaToppingRepository.getNotNull(id);
		Topping editBy = pizzaToppingMapper.createNewTopping(toppingDto);
		pizzaToppingMapper.edit(toEdit, editBy);
		validator.validateBeforeEditing(toEdit);
		Topping save = pizzaToppingRepository.save(toEdit);
		return pizzaToppingMapper.entityToDto(save);
	}

	@Override
	public void deleteTopping(UUID id) {
		pizzaToppingRepository.deleteById(id);
	}

	@Override
	public PizzaToppingDto changeImage(UUID id, UUID imageId) {
		File file = imageDbRepository.getNotNull(imageId);
		Topping topping = pizzaToppingRepository.getNotNull(id);
		topping.setImage(file);
		return pizzaToppingMapper.entityToDto(pizzaToppingRepository.save(topping));
	}

	@Override
	public PizzaToppingDto deleteImage(UUID id) {
		Topping topping = pizzaToppingRepository.getNotNull(id);
		topping.setImage(null);
		return pizzaToppingMapper.entityToDto(pizzaToppingRepository.save(topping));
	}

}
