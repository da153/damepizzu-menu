package com.example.springsocial.service.impl;

import com.example.springsocial.mapper.PizzaBaseMapper;
import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.model.dto.PizzaBaseDto;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;
import com.example.springsocial.repository.PizzaBaseRepository;
import com.example.springsocial.service.PizzaBaseService;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@AllArgsConstructor
public class PizzaBaseServiceImpl implements PizzaBaseService {

	private final PizzaBaseRepository pizzaBaseRepository;

	private final PizzaBaseMapper pizzaBaseMapper;

	private final Validator<PizzaBase> validator;

	@Override
	public List<PizzaBaseDto> findAll(String search) {
		List<PizzaBase> pizzaBases = pizzaBaseRepository.findAll(search);
		return pizzaBaseMapper.entitiesToDtos(pizzaBases);
	}

	@Override
	public PizzaBaseDto findById(UUID id) {
		PizzaBase pizzaBase = pizzaBaseRepository.getNotNull(id);
		return pizzaBaseMapper.entityToDto(pizzaBase);
	}

	@Override
	public PizzaBaseDto createNewPizzaBase(NewPizzaBaseDto pizzaBaseDto) {
		PizzaBase pizzaBase = pizzaBaseMapper.createNewPizzaBase(pizzaBaseDto);
		validator.validateBeforeCreation(pizzaBase);
		pizzaBase = pizzaBaseRepository.save(pizzaBase);
		return pizzaBaseMapper.entityToDto(pizzaBase);
	}

	@Override
	public PizzaBaseDto editPizzaBase(NewPizzaBaseDto pizzaBaseDto, UUID id) {
		PizzaBase toEdit = pizzaBaseRepository.getNotNull(id);
		PizzaBase editBy = pizzaBaseMapper.createNewPizzaBase(pizzaBaseDto);
		pizzaBaseMapper.edit(toEdit, editBy);
		validator.validateBeforeEditing(toEdit);
		PizzaBase edited = pizzaBaseRepository.save(toEdit);
		return pizzaBaseMapper.entityToDto(edited);
	}

	@Override
	public void deletePizzaBase(UUID id) {
		pizzaBaseRepository.deleteById(id);
	}

}
