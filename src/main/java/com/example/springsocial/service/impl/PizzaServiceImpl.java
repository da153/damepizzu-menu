package com.example.springsocial.service.impl;

import com.example.springsocial.mapper.PizzaMapper;
import com.example.springsocial.model.Pizza;
import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.model.Topping;
import com.example.springsocial.model.dto.PizzaDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;
import com.example.springsocial.repository.PizzaBaseRepository;
import com.example.springsocial.repository.PizzaRepository;
import com.example.springsocial.repository.PizzaToppingRepository;
import com.example.springsocial.service.PizzaService;
import com.example.springsocial.validator.Validator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class PizzaServiceImpl implements PizzaService {

	private final PizzaRepository pizzaRepository;

	private final PizzaToppingRepository pizzaToppingRepository;

	private final PizzaBaseRepository pizzaBaseRepository;

	private final PizzaMapper pizzaMapper;

	private final Validator<Pizza> validator;

	@Override
	public List<PizzaDto> findPizzasByIds(String[] ids) {
		List<UUID> uuids = Arrays.stream(ids)
			.map(UUID::fromString)
			.collect(Collectors.toList());
		pizzaRepository.existsAllByIdIn(uuids);
		List<Pizza> pizzas = pizzaRepository.findByIdIn(uuids);
		return pizzaMapper.entitiesToDtos(pizzas);
	}

	@Override
	public PizzaDto findPizzaById(UUID id) {
		Pizza pizza = pizzaRepository.getNotNull(id);
		return pizzaMapper.entityToDto(pizza);
	}

	@Override
	public List<PizzaDto> findAllPizzas(String search) {
		List<Pizza> pizzas = pizzaRepository.findAll(search);
		return pizzaMapper.entitiesToDtos(pizzas);
	}

	@Override
	public PizzaDto createNewPizza(NewPizzaDto pizzaDto) {
		Pizza pizza = pizzaMapper.createNewPizza(pizzaDto);
		validator.validateBeforeCreation(pizza);
		pizza = pizzaRepository.save(pizza);
		return pizzaMapper.entityToDto(pizza);
	}

	@Override
	public PizzaDto edditPizza(NewPizzaDto pizzaDto, UUID id) {
		Pizza toEdit = pizzaRepository.getNotNull(id);
		Pizza editBy = pizzaMapper.createNewPizza(pizzaDto);
		pizzaMapper.edit(toEdit, editBy);
		validator.validateBeforeEditing(toEdit);
		Pizza edited = pizzaRepository.save(toEdit);
		return pizzaMapper.entityToDto(edited);
	}

	@Override
	public PizzaDto addToppingOnPizza(UUID id, UUID toppingId) {
		Pizza pizza = pizzaRepository.getNotNull(id);
		Topping topping = pizzaToppingRepository.getNotNull(toppingId);
		pizza.getToppings().add(topping);
		return pizzaMapper.entityToDto(pizzaRepository.save(pizza));
	}

	@Override
	public PizzaDto deleteToppingFromPizza(UUID id, UUID toppingId) {
		Pizza pizza = pizzaRepository.getNotNull(id);
		List<Topping> toppings = pizza.getToppings()
			.stream()
			.filter(t -> !t.getId()
				.equals(toppingId))
			.collect(Collectors.toList());
		pizza.setToppings(toppings);
		return pizzaMapper.entityToDto(pizzaRepository.save(pizza));
	}

	@Override
	public void deletePizza(UUID id) {
		pizzaRepository.deleteById(id);
	}

	@Override
	public PizzaDto changePizzaBase(UUID id, UUID baseId) {
		Pizza pizza = pizzaRepository.getNotNull(id);
		PizzaBase pizzaBase = pizzaBaseRepository.getNotNull(baseId);
		pizza.setPizzaBase(pizzaBase);
		return pizzaMapper.entityToDto(pizzaRepository.save(pizza));
	}

}
