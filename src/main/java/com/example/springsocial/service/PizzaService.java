package com.example.springsocial.service;

import com.example.springsocial.model.dto.PizzaDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;

import java.util.List;
import java.util.UUID;

public interface PizzaService {

	List<PizzaDto> findPizzasByIds(String[] ids);

	PizzaDto findPizzaById(UUID id);

	List<PizzaDto> findAllPizzas(String search);

	PizzaDto createNewPizza(NewPizzaDto pizzaDto);

	PizzaDto edditPizza(NewPizzaDto pizzaDto, UUID id);

	public PizzaDto addToppingOnPizza(UUID id, UUID toppingId);

	public PizzaDto deleteToppingFromPizza(UUID id, UUID toppingId);

	public void deletePizza(UUID id);

	public PizzaDto changePizzaBase(UUID id, UUID baseId);
}
