package com.example.springsocial.service;

import com.example.springsocial.model.dto.PizzaBaseDto;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;

import java.util.List;
import java.util.UUID;

public interface PizzaBaseService {

	List<PizzaBaseDto> findAll(String search);

	PizzaBaseDto findById(UUID id);

	PizzaBaseDto createNewPizzaBase(NewPizzaBaseDto pizzaBaseDto);

	PizzaBaseDto editPizzaBase(NewPizzaBaseDto pizzaBaseDto, UUID id);

	void deletePizzaBase(UUID id);
}
