package com.example.springsocial.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pizza {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;

	private String name;

	private String description;

	private Double price;

	@ManyToOne
	private PizzaBase pizzaBase;

	@ManyToMany
	@JoinTable(name = "pizza_topping", joinColumns = {@JoinColumn(name = "pizza_id")}, inverseJoinColumns = {@JoinColumn(name = "topping_id")})
	private List<Topping> toppings;

	@ManyToOne(fetch = FetchType.EAGER)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private File image;

}
