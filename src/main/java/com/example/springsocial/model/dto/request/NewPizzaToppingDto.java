package com.example.springsocial.model.dto.request;

import com.example.springsocial.model.dto.FileDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewPizzaToppingDto {
	private String name;

	private String description;

	private Double price;

	private UUID image;
}
