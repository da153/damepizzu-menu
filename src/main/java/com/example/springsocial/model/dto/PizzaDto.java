package com.example.springsocial.model.dto;

import com.example.springsocial.model.PizzaBase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaDto {

	private UUID id;

	private String name;

	private String description;

	private Double price;

	private List<PizzaToppingDto> toppings;

	private PizzaBase pizzaBase;

	private FileDto image;
}
