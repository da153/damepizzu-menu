package com.example.springsocial.model.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewPizzaDto {

	private String name;

	private String description;

	private Double price;

	private UUID pizzaBase;

	private List<UUID> toppings;

	private UUID image;
}
