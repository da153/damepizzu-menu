package com.example.springsocial.model.dto;

import com.example.springsocial.model.File;
import com.example.springsocial.model.Pizza;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaToppingDto {

	private UUID id;

	private String name;

	private String description;

	private Double price;

	private FileDto image;
}
