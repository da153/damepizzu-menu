package com.example.springsocial.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PizzaBaseDto {

	private UUID id;

	private String name;

	private String description;

	private Double extraPrice;
}
