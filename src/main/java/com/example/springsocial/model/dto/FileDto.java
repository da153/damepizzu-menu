package com.example.springsocial.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileDto {

	public UUID id;

	private String name;

	private String type;

	private Long size;

	private String url;

}
