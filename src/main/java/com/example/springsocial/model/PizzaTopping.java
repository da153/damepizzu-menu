package com.example.springsocial.model;


import com.example.springsocial.model.id.PizzaToppingId;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Embeddable
public class PizzaTopping {

	@EmbeddedId
	private PizzaToppingId id;

	@ManyToOne
	@MapsId("pizzaId")
	@JoinColumn(name = "pizza_id")
	Pizza pizza;

	@ManyToOne
	@MapsId("toppingId")
	@JoinColumn(name = "topping_id")
	Topping topping;
}
