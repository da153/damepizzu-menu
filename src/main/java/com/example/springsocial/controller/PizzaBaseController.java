package com.example.springsocial.controller;

import com.example.springsocial.model.dto.PizzaBaseDto;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping("/api/pizza-base")
public interface PizzaBaseController {

	@GetMapping
	List<PizzaBaseDto> findAll(@RequestParam(value = "search", required = false) String search);

	@GetMapping("/{id}")
	PizzaBaseDto findById(@PathVariable UUID id);

	@PostMapping
	PizzaBaseDto createNewPizzaBase(@Valid @RequestBody NewPizzaBaseDto pizzaBaseDto);

	@PutMapping("/{id}")
	PizzaBaseDto editPizzaBase(@Valid @RequestBody NewPizzaBaseDto pizzaBaseDto, @PathVariable UUID id);

	@DeleteMapping("/{id}")
	void deletePizzaBase(@PathVariable UUID id);

}
