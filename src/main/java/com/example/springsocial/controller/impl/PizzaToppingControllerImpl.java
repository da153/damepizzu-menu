package com.example.springsocial.controller.impl;

import com.example.springsocial.controller.ToppingController;
import com.example.springsocial.model.dto.PizzaToppingDto;
import com.example.springsocial.model.dto.request.NewPizzaToppingDto;
import com.example.springsocial.service.PizzaToppingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class PizzaToppingControllerImpl implements ToppingController {

	private final PizzaToppingService pizzaToppingService;

	@Override
	public List<PizzaToppingDto> findAll(String search) {
		return pizzaToppingService.findAll(search);
	}

	@Override
	public PizzaToppingDto findById(UUID id) {
		return pizzaToppingService.findById(id);
	}

	@Override
	public PizzaToppingDto createNewTopping(NewPizzaToppingDto toppingDto) {
		return pizzaToppingService.createNewTopping(toppingDto);
	}

	@Override
	public PizzaToppingDto editTopping(NewPizzaToppingDto toppingDto, UUID id) {
		return pizzaToppingService.editTopping(toppingDto, id);
	}

	@Override
	public void deleteTopping(UUID id) {
		pizzaToppingService.deleteTopping(id);
	}

	@Override
	public PizzaToppingDto changeImage(UUID id, UUID imageId) {
		return pizzaToppingService.changeImage(id, imageId);
	}

	@Override
	public PizzaToppingDto deleteImage(UUID id) {
		return pizzaToppingService.deleteImage(id);
	}

}
