package com.example.springsocial.controller.impl;

import com.example.springsocial.controller.PizzaBaseController;
import com.example.springsocial.model.dto.PizzaBaseDto;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;
import com.example.springsocial.service.PizzaBaseService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class PizzaBaseControllerImpl implements PizzaBaseController {

	private final PizzaBaseService pizzaBaseService;

	@Override
	public List<PizzaBaseDto> findAll(String search) {
		return pizzaBaseService.findAll(search);
	}

	@Override
	public PizzaBaseDto findById(UUID id) {
		return pizzaBaseService.findById(id);
	}

	@Override
	public PizzaBaseDto createNewPizzaBase(NewPizzaBaseDto pizzaBaseDto) {
		return pizzaBaseService.createNewPizzaBase(pizzaBaseDto);
	}

	@Override
	public PizzaBaseDto editPizzaBase(NewPizzaBaseDto pizzaBaseDto, UUID id) {
		return pizzaBaseService.editPizzaBase(pizzaBaseDto, id);
	}

	@Override
	public void deletePizzaBase(UUID id) {
		pizzaBaseService.deletePizzaBase(id);
	}

}
