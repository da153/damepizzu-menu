package com.example.springsocial.controller.impl;

import com.example.springsocial.controller.PizzaController;
import com.example.springsocial.model.dto.PizzaDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;
import com.example.springsocial.service.PizzaService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class PizzaControllerImpl implements PizzaController {

	private final PizzaService pizzaService;

	@Override
	public PizzaDto findPizzaById(UUID id) {
		return pizzaService.findPizzaById(id);
	}

	@Override
	public List<PizzaDto> findPizzasByIds(String[] ids) {
		return pizzaService.findPizzasByIds(ids);
	}

	@Override
	public List<PizzaDto> findAllPizzas(String search) {
		return pizzaService.findAllPizzas(search);
	}

	@Override
	public PizzaDto createNewPizza(NewPizzaDto pizzaDto) {
		return pizzaService.createNewPizza(pizzaDto);
	}

	@Override
	public PizzaDto edditPizza(NewPizzaDto pizzaDto, UUID id) {
		return pizzaService.edditPizza(pizzaDto, id);
	}

	@Override
	public PizzaDto addToppingOnPizza(UUID id, UUID toppingId) {
		return pizzaService.addToppingOnPizza(id, toppingId);
	}

	@Override
	public PizzaDto deleteToppingFromPizza(UUID id, UUID toppingId) {
		return pizzaService.deleteToppingFromPizza(id, toppingId);
	}

	@Override
	public PizzaDto changePizzaBase(UUID id, UUID baseId) {
		return null;
	}

	@Override
	public void deletePizza(UUID id) {
		pizzaService.deletePizza(id);
	}

}
