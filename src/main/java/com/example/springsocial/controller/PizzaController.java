package com.example.springsocial.controller;

import com.example.springsocial.model.Pizza;
import com.example.springsocial.model.dto.PizzaDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@RequestMapping("/api/v1/pizza")
public interface PizzaController {

	@GetMapping("/{id}")
	PizzaDto findPizzaById(@PathVariable UUID id);

	@GetMapping("/all")
	List<PizzaDto> findPizzasByIds(@RequestParam("pizzaId") String[] ids);

	@GetMapping
	List<PizzaDto> findAllPizzas(@RequestParam(value = "search", required = false) String search);

	@PostMapping
	PizzaDto createNewPizza(@Valid @RequestBody NewPizzaDto pizzaDto);

	@PutMapping("/{id}")
	PizzaDto edditPizza(@Valid @RequestBody NewPizzaDto pizzaDto, @PathVariable UUID id);

	@PostMapping("/{id}/topping/{toppingId}/add")
	PizzaDto addToppingOnPizza(@PathVariable UUID id, @PathVariable UUID toppingId);

	@DeleteMapping("/{id}/topping/{toppingId}/delete")
	PizzaDto deleteToppingFromPizza(@PathVariable UUID id, @PathVariable UUID toppingId);

	@PostMapping("/{id}/base/{baseId}/change")
	PizzaDto changePizzaBase(@PathVariable UUID id, @PathVariable UUID baseId);

	@DeleteMapping("/{id}")
	void deletePizza(@PathVariable UUID id);


}
