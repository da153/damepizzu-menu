package com.example.springsocial.controller;

import com.example.springsocial.exception.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({BadRequestException.class})
	public final ResponseEntity<?> handleGeneralException(Exception ex, WebRequest request) {
		ex.printStackTrace();
		return ResponseEntity.status(400)
			.body(ex.getMessage());
	}

	/**
	 * Provides handling for exceptions throughout this service.
	 */
	@ExceptionHandler({BadCredentialsException.class})
	public final ResponseEntity<?> handleException(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status)
			.build();
	}

}
