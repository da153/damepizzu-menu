package com.example.springsocial.controller;

import com.example.springsocial.model.dto.PizzaBaseDto;
import com.example.springsocial.model.dto.PizzaToppingDto;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;
import com.example.springsocial.model.dto.request.NewPizzaToppingDto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping("/api/topping")
public interface ToppingController {

	@GetMapping
	List<PizzaToppingDto> findAll(@RequestParam(value = "search", required = false) String search);

	@GetMapping("/{id}")
	PizzaToppingDto findById(@PathVariable UUID id);

	@PostMapping
	PizzaToppingDto createNewTopping(@Valid @RequestBody NewPizzaToppingDto toppingDto);

	@PutMapping("/{id}")
	PizzaToppingDto editTopping(@Valid @RequestBody NewPizzaToppingDto toppingDto, @PathVariable UUID id);

	@DeleteMapping("/{id}")
	void deleteTopping(@PathVariable UUID id);

	@PostMapping("/{id}/image/{imageId}/change")
	PizzaToppingDto changeImage(@PathVariable UUID id, @PathVariable UUID imageId);

	@DeleteMapping("/{id}/image/delete")
	PizzaToppingDto deleteImage(@PathVariable UUID id);
}
