package com.example.springsocial.controller;

import com.example.springsocial.model.dto.FileDto;
import com.example.springsocial.model.dto.PageDto;
import com.example.springsocial.model.enumerated.GeneralSortType;
import com.example.springsocial.service.FileService;
import com.example.springsocial.util.Tuple;
import io.minio.ObjectStat;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class FileControllerImpl implements FileController {

	private final FileService fileService;

	@Override
	public FileDto uploadFile(MultipartFile file) {
		return fileService.uploadFile(file);
	}

	@Override
	public PageDto<FileDto> getAllFiles(Integer pageNumber,
	                                    Integer pageSize,
	                                    String searchQuery,
	                                    GeneralSortType sortType) {
		return fileService.getAllFiles(pageNumber, pageSize, searchQuery, sortType);
	}


	@Override
	public FileDto getFile(UUID id) {
		return fileService.getFile(id);
	}

	@Override
	public void deleteFile(UUID id) {
		fileService.deleteFile(id);
	}

	@Override
	public ResponseEntity<byte[]> serveFile(UUID id, HttpServletResponse response) {
		Tuple<byte[], ObjectStat> withMetadata = fileService.serveFile(id);
		return ResponseEntity.ok()
			.headers(new HttpHeaders(new LinkedMultiValueMap<>(withMetadata.getRight()
				.httpHeaders())))
			.body(withMetadata.getLeft());

	}

}
