package com.example.springsocial.mapper;


import com.example.springsocial.mapper.decorator.FileMapperDecorator;
import com.example.springsocial.mapper.decorator.PizzaMapperDecorator;
import com.example.springsocial.model.Pizza;
import com.example.springsocial.model.dto.PizzaDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FileMapper.class, PizzaToppingMapper.class, PizzaBaseMapper.class})
@DecoratedWith(PizzaMapperDecorator.class)
public interface PizzaMapper extends EditingMapper<Pizza> {

	PizzaDto entityToDto(Pizza pizza);

	List<PizzaDto> entitiesToDtos(List<Pizza> pizzaList);

	@Mapping(target = "pizzaBase", ignore = true)
	@Mapping(target = "toppings", ignore = true)
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "image", ignore = true)
	Pizza createNewPizza (NewPizzaDto pizzaDto);
}
