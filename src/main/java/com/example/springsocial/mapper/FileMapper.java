package com.example.springsocial.mapper;

import com.example.springsocial.mapper.decorator.FileMapperDecorator;
import com.example.springsocial.model.File;
import com.example.springsocial.model.dto.FileDto;
import io.minio.ObjectStat;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(FileMapperDecorator.class)
public interface FileMapper extends EditingMapper<File> {

	@Mapping(target = "url", ignore = true)
	FileDto entityToDto(File file);

	List<FileDto> entitiesToDto(List<File> files);

	File fileAndMetadataToDbEntity(MultipartFile file, ObjectStat metadata);

}
