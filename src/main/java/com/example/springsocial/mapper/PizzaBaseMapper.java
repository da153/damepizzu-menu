package com.example.springsocial.mapper;

import com.example.springsocial.mapper.decorator.PizzaBaseMapperDecorator;
import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.model.dto.PizzaBaseDto;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FileMapper.class, PizzaMapper.class})
@DecoratedWith(PizzaBaseMapperDecorator.class)
public interface PizzaBaseMapper extends EditingMapper<PizzaBase> {

	PizzaBaseDto entityToDto(PizzaBase base);

	List<PizzaBaseDto> entitiesToDtos(List<PizzaBase> pizzas);

	PizzaBase createNewPizzaBase(NewPizzaBaseDto pizzaBaseDto);
}
