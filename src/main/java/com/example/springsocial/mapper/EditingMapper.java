package com.example.springsocial.mapper;

public interface EditingMapper<T> {

	default void edit(T toEdit, T editBy) {
		//uses decorator implementation for Mapstruct mapping
	}

}
