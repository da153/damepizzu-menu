package com.example.springsocial.mapper;

import com.example.springsocial.model.dto.PageDto;
import org.springframework.data.domain.Page;

public class PageMetaInfoMapper {

	public static <T> PageDto<T> mapPageMetaInfo(Page<?> page) {
		PageDto<T> pageDto = new PageDto<>();
		pageDto.setPageNumber(page.getNumber());
		pageDto.setTotalPages(page.getTotalPages());
		pageDto.setHasNextPage(page.hasNext());
		pageDto.setElementsPerPage(page.getPageable()
			.getPageSize());
		pageDto.setNumberOfElements(page.getNumberOfElements());
		pageDto.setTotalNumberOfElements((int) page.getTotalElements());
		return pageDto;
	}

}
