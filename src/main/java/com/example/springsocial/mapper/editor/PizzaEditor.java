package com.example.springsocial.mapper.editor;

import com.example.springsocial.model.Pizza;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PizzaEditor implements Editor<Pizza> {

	@Override
	public void edit(Pizza toEdit, Pizza editBy) {
		toEdit.setName(editBy.getName());
		toEdit.setDescription(editBy.getDescription());
		toEdit.setImage(editBy.getImage());
		toEdit.setToppings(editBy.getToppings());
		toEdit.setPrice(editBy.getPrice());
		toEdit.setPizzaBase(editBy.getPizzaBase());
	}

}
