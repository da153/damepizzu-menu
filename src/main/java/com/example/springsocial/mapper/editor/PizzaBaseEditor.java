package com.example.springsocial.mapper.editor;

import com.example.springsocial.model.PizzaBase;
import org.springframework.stereotype.Component;

@Component
public class PizzaBaseEditor implements Editor<PizzaBase> {

	@Override
	public void edit(PizzaBase toEdit, PizzaBase editBy) {
		toEdit.setDescription(editBy.getDescription());
		toEdit.setExtraPrice(editBy.getExtraPrice());
		toEdit.setName(editBy.getName());
	}

}
