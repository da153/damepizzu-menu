package com.example.springsocial.mapper.editor;

import com.example.springsocial.model.Topping;
import org.springframework.stereotype.Component;

@Component
public class PizzaToppingEditor implements Editor<Topping> {

	@Override
	public void edit(Topping toEdit, Topping editBy) {
		toEdit.setName(editBy.getName());
		toEdit.setDescription(editBy.getDescription());
		toEdit.setPrice(editBy.getPrice());
		toEdit.setImage(editBy.getImage());
	}

}
