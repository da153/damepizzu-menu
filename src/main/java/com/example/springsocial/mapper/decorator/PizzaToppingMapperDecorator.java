package com.example.springsocial.mapper.decorator;

import com.example.springsocial.mapper.FileMapper;
import com.example.springsocial.mapper.PizzaToppingMapper;
import com.example.springsocial.model.File;
import com.example.springsocial.model.Topping;
import com.example.springsocial.model.dto.PizzaToppingDto;
import com.example.springsocial.model.dto.request.NewPizzaToppingDto;
import com.example.springsocial.repository.impl.ImageDbRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class PizzaToppingMapperDecorator extends EditingMapperImpl<Topping> implements PizzaToppingMapper {

	@Autowired
	private PizzaToppingMapper delegate;

	@Autowired
	private FileMapper fileMapper;

	@Autowired
	private ImageDbRepository imageDbRepository;

	@Override
	public Topping createNewTopping(NewPizzaToppingDto newPizzaToppingDto) {
		Topping topping = delegate.createNewTopping(newPizzaToppingDto);
		if (newPizzaToppingDto.getImage() != null) {
			File file = imageDbRepository.getNotNull(newPizzaToppingDto.getImage());
			topping.setImage(file);
		}
		return topping;
	}

	@Override
	public PizzaToppingDto entityToDto(Topping topping) {
		PizzaToppingDto pizzaToppingDto = delegate.entityToDto(topping);
		if (topping.getImage() != null) {
			pizzaToppingDto.setImage(fileMapper.entityToDto(topping.getImage()));
		}
		return pizzaToppingDto;
	}

}
