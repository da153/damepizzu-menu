package com.example.springsocial.mapper.decorator;

import com.example.springsocial.mapper.PizzaBaseMapper;
import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.model.dto.request.NewPizzaBaseDto;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class PizzaBaseMapperDecorator extends EditingMapperImpl<PizzaBase> implements PizzaBaseMapper {

	@Autowired
	private PizzaBaseMapper delegate;


}
