package com.example.springsocial.mapper.decorator;

import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.mapper.PizzaMapper;
import com.example.springsocial.model.File;
import com.example.springsocial.model.Pizza;
import com.example.springsocial.model.PizzaBase;
import com.example.springsocial.model.PizzaTopping;
import com.example.springsocial.model.Topping;
import com.example.springsocial.model.dto.PizzaDto;
import com.example.springsocial.model.dto.request.NewPizzaDto;
import com.example.springsocial.repository.FileStorage;
import com.example.springsocial.repository.PizzaBaseRepository;
import com.example.springsocial.repository.PizzaToppingRepository;
import com.example.springsocial.repository.impl.ImageDbRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class PizzaMapperDecorator extends EditingMapperImpl<Pizza> implements PizzaMapper {

	@Autowired
	private PizzaToppingRepository pizzaToppingRepository;

	@Autowired
	private PizzaBaseRepository pizzaBaseRepository;

	@Autowired
	private ImageDbRepository imageRepository;

	@Autowired
	private PizzaMapper delegate;

	@Override
	public Pizza createNewPizza(NewPizzaDto pizzaDto) {
		Pizza pizza = delegate.createNewPizza(pizzaDto);
		List<Topping> toppings = findToppingByListOfIds(pizzaDto.getToppings());
		pizza.setToppings(toppings);
		try {
			PizzaBase pizzaBase = pizzaBaseRepository.getNotNull(pizzaDto.getPizzaBase());
			pizza.setPizzaBase(pizzaBase);
		} catch (Exception  e) {
			e.printStackTrace();
			throw new BadRequestException(String.format("Pizza base with id %s cannot be found!", pizzaDto.getPizzaBase().toString()));
		}
		if (pizzaDto.getImage() != null) {
			try {
				File file = imageRepository.getNotNull(pizzaDto.getImage());
				pizza.setImage(file);
			} catch (Exception e){
				e.printStackTrace();
				throw new BadRequestException(String.format("Image with id %s cannot be found!", pizzaDto.getImage().toString()));
			}
		}
		return pizza;
	}

	private List<Topping> findToppingByListOfIds(List<UUID> uuids) {
		 return uuids.stream().map(uuid -> {
			 try {
				 return pizzaToppingRepository.getNotNull(uuid);
			 } catch (Exception e) {
				 e.printStackTrace();
				 throw new BadRequestException(String.format("Pizza topping with id %s cannot be found!", uuid.toString()));
			 }
		 }).collect(Collectors.toList());
	}

}
