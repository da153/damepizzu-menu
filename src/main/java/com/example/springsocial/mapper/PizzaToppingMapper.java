package com.example.springsocial.mapper;


import com.example.springsocial.mapper.decorator.FileMapperDecorator;
import com.example.springsocial.mapper.decorator.PizzaToppingMapperDecorator;
import com.example.springsocial.model.PizzaTopping;
import com.example.springsocial.model.Topping;
import com.example.springsocial.model.dto.PizzaToppingDto;
import com.example.springsocial.model.dto.request.NewPizzaToppingDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FileMapper.class, PizzaToppingMapper.class})
@DecoratedWith(PizzaToppingMapperDecorator.class)
public interface PizzaToppingMapper extends EditingMapper<Topping> {

	PizzaToppingDto entityToDto(Topping topping);

	List<PizzaToppingDto> entitiesToDtos(List<Topping> toppings);

	@Mapping(target = "image", ignore = true)
	@Mapping(target = "pizzas", ignore = true)
	@Mapping(target = "id", ignore = true)
	Topping createNewTopping(NewPizzaToppingDto newPizzaToppingDto);
}
